import React from 'react'

export default function Footer() {
    return(
        <footer className="text-center">
            <h6>All rights reserved 2022</h6>
            <h6>Zuitt Coding Bootcamp</h6>
        </footer>
    )
}